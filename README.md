# ratelimited-api

NodeJS API with a Redis implemeted rate limiter

```diff
- Current Version: 1.0.1
```

## Installation

Install dependencies and start the app.

```sh
npm install
npm start
```

## Environment

Explanation for every available environment variable
| Variable | Usage |
| ------------ | -------------------------------------------------------------- |
| API_PORT | **integer** Api port |
| REDIS_URI | **string** Redis connection string |
| JWT_SECRET | **string** JWT secret |
| RATE_LIMIT | **integet** Number of requests allowed per 10 seconds|
