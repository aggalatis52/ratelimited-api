const jwt = require('jsonwebtoken')
const JWT_SECRET = process.env.JWT_SECRET

function generateToken(data) {
	return jwt.sign(data, JWT_SECRET, { expiresIn: 60 * 60 * 24 })
}

function verifyToken(token) {
	return new Promise((resolve, reject) => {
		jwt.verify(token, JWT_SECRET, (err, decoded) => {
			if (err) reject()
			resolve(decoded)
		})
	})
}

function getIpFromRequest(ipAddress) {
	return ipAddress.substr(ipAddress.lastIndexOf(':') + 1)
}

module.exports = {
	generateToken,
	verifyToken,
	getIpFromRequest
}
