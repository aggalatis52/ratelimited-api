const redis = require('ioredis')

class Redis {
	constructor() {
		this.client = null
		this.uri = process.env.REDIS_URI
	}

	async init() {
		await this.connect()
	}

	async connect() {
		return new Promise((resolve, reject) => {
			this.client = new redis(this.uri)

			this.client.on('ready', () => {
				console.log(`Redis Connected: ${this.uri}`)
				resolve(true)
			})

			this.client.on('error', (error) => {
				this.client = null
				console.log(`Redis error: ${error}`)
				process.exit(0)
			})
		})
	}

	async setKey(key, content, expiration) {
		await this.client.set(key, JSON.stringify(content), 'EX', expiration)
	}

	async incrementKey(key) {
		const times = await this.client.incr(key)
		if (times == 1) await this.expireKey(key, 10)
		return times
	}

	async expireKey(key, seconds) {
		await this.client.expire(key, seconds)
	}

	async setKeyWithoutTTL(key, content) {
		await this.client.set(key, JSON.stringify(content), 'KEEPTTL')
	}

	async getKey(key) {
		const cached_key = await this.client.get(key)
		return JSON.parse(cached_key)
	}

	async delKey(key) {
		await this.client.del(key)
	}

	async getKeys(pattern) {
		return await this.client.keys(pattern)
	}
}

module.exports = new Redis()
