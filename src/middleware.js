const Redis = require('./redis')
const helpers = require('./helpers')

const RATE_LIMIT = parseInt(process.env.RATE_LIMIT)

const auth = async (req, res, next) => {
	try {
		let authHeader = req.headers['authorization']
		await helpers.verifyToken(authHeader.replace('Bearer', '').trim())
		return next()
	} catch (error) {
		console.log(error)
		res.status(401).send()
	}
}

const limit = async (req, res, next) => {
	const ipAddress = helpers.getIpFromRequest(req.ip)
	const timestamp = parseInt(+Date.now() / 10000)
	const cacheKey = `${ipAddress}:${timestamp}`
	const requestTimes = await Redis.incrementKey(cacheKey)
	if (requestTimes > RATE_LIMIT) {
		res.status(429).send()
		return
	}
	return next()
}

module.exports = {
	auth,
	limit
}
