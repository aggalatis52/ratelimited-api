const fs = require('fs')
const helpers = require('./helpers')

const authenticate = async (req, res) => {
	const payload = req.body
	const ip = helpers.getIpFromRequest(req.ip)
	payload.ip = ip
	if (!payload.username || !payload.password) {
		res.status(400).send()
		return
	}
	const token = helpers.generateToken(payload)
	res.status(200).send(token)
}

const readMessage = async (req, res) => {
	fs.readFile(`./messages/message_${req.params.id}.json`, (err, data) => {
		if (err) {
			res.status(500).send(err)
		} else {
			res.status(200).send(data)
		}
	})
}

module.exports = {
	authenticate,
	readMessage
}
