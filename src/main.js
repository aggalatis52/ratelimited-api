require('dotenv').config()
const express = require('express')
const Redis = require('./redis')

const PORT = parseInt(process.env.API_PORT)

async function start() {
	await Redis.init()
	const app = express()
	app.use(express.json({ limit: '1000mb' }))

	app.use('/api', require('./routes'))

	app.listen(PORT, () => {
		console.log(`Rate Limiter API listening on ${PORT}`)
	})
}

start()
