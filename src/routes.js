const express = require('express')
const { authenticate, readMessage } = require('./controllers')
const { auth, limit } = require('./middleware')
const router = express.Router()

router.post('/auth', authenticate)
router.get('/message/:id', auth, limit, readMessage)

module.exports = router
